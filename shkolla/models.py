from django.contrib.auth.models import User
from django.db import models


class Post(models.Model):
    title = models.CharField(max_length=250)
    slug = models.SlugField()
    intro = models.TextField()
    body = models.TextField()
    add_date = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['add_date']


class Comment(models.Model):
    post = models.ForeignKey(Post, related_name='comments', on_delete=models.CASCADE)
    name = models.CharField(max_length=200)
    email = models.EmailField()
    body = models.TextField()
    add_date = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['add_date']






    # class Meta:
    #     verbose_name = 'Post'
    #     verbose_name_plural = 'Posts'
    #     db_table = 'post'

    # first_name = models.CharField('Emri', max_length=20)
    # last_name = models.CharField('Mbiemri', max_length=20)
    # email = models.EmailField('Email', max_length=20)
    # date_of_post = models.CharField('Data e postimit', max_length=20)
    # school = models.CharField('Shkolla', max_length=20, blank=True)
    # position = models.CharField('Posicioni', max_length=20, blank=True)
    # user = models.ForeignKey(User, on_delete=models.CASCADE)
    # 
    # def __str__(self):
    #     return '{} {}'.format(self.first_name, self.last_name)
