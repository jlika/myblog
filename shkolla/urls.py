from django.urls import path

from shkolla.views import home, post_detail

urlpatterns = [
    path('', home, name='home'),
    path('<slug:slug>/', post_detail, name='post_detail'),
    # path('post/', add_post, name='add_post'),

]
