import logging

from django.contrib.auth import authenticate, login
from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import render, redirect
from django.http import HttpResponse

from shkolla.forms import CommentForm
from shkolla.models import Post


def home(request):
    posts = Post.objects.all()
    return render(request, 'home.html', {'posts': posts})


def post_detail(request, slug):
    post = Post.objects.get(slug=slug)
    if request.method == 'POST':
        form = CommentForm(request.POST)
        if form.is_valid():
            comment = form.save(commit=False)
            comment.post = post
            comment.save()
            return redirect('post_detail', slug=post.slug)
    else:
        form = CommentForm()
    return render(request, 'post_detail.html', {'post': post, 'form': form})


# def add_post(request, slug):
#     post = Post.objects.get(slug=slug)
#     if request.method == 'POST':
#         form = AddPostForm(request.POST)
#         if form.is_valid():
#             post = form.save(commit=False)
#             post.post = post
#             post.save()
#             return redirect('home', slug=post.slug)
#     else:
#         form = AddPostForm()
#     return render(request, 'add_post.html', {'post': post, 'form': form})


def signup(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('home')
    else:
        form = UserCreationForm()
    return render(request, 'signup.html', {'form': form})

# class PostListView(ListView):
#     model = Post
#     template_name = 'home.html'
#     #
#     # def head(self, *args, **kwargs):
#     #     last_post = self.get_queryset().latest('publication_date')
#     #     response = HttpResponse()
#     #     # RFC 1123 date format
#     #     response['Last-Modified'] = last_post.publication_date.strftime('%a, %d %b %Y %H:%M:%S GMT')
#     #     return response
