from django.apps import AppConfig


class ShkollaConfig(AppConfig):
    name = 'shkolla'
