from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include

from shkolla.views import signup

urlpatterns = [
                  path('admin/', admin.site.urls),
                  path('', include('shkolla.urls')),
                  path('users/', include('django.contrib.auth.urls')),
                  path('signup/',  signup, name='signup'),
              ] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
